<?php

require_once('../../config.php');
require_once("$CFG->dirroot/enrol/locallib.php");
global $OUTPUT, $DB, $CFG, $USER, $PAGE;
$courseid = required_param('courseid', PARAM_INT);
$blockid = required_param('blockid', PARAM_INT);
$id = optional_param('id', 0, PARAM_INT);

if(!($course = $DB->get_record('course', array('id' => $courseid)))) {
	print_error('invalidcourse', 'block_student_status', $courseid);
}
$context = context_course::instance($course->id, MUST_EXIST);
$heading = "Student Records";
require_login($courseid);
$url = new moodle_url($CFG->wwwroot . '/blocks/student_status/view.php', array('id' => $course->id));
$link = new moodle_url($CFG->wwwroot . '/report/log/user.php');
$PAGE->set_url($url);
$PAGE->set_heading($heading);
$PAGE->set_context($context);
$PAGE->set_pagelayout('standard');
$r = $DB->get_record('role', array('shortname' => 'student'));
$roleid = $r->id;
$now = time();
echo $OUTPUT->header();

$sql = "select u.id, u.username, u.lastaccess, COALESCE(ul.timeaccess, 0) AS lastcourseaccess, ra.roleid, ul.courseid from {role_assignments} ra, {user} u, mdl_role r, {user_lastaccess} ul where u.id = ra.userid and ra.roleid = r.id and u.id = ul.userid and ra.contextid = ? and ra.roleid = ? and ul.courseid = ?;";
$params = array($context->id, $roleid, $courseid);
$result = $DB->get_records_sql($sql, $params);
$time = array();
$i = 0;
foreach ($result as $r) {
if($r->lastcourseaccess == 0){
	$time[$i] = "Never";
}
else{
    $time[$i] = format_time($now - $r->lastcourseaccess);
	++$i;
}
}
$html = '';
	$html .= html_writer::start_tag('div', array('id' => 'student_section'));
	$html .= html_writer::start_tag('table', array('id' => 'student_table', 'class' => 'generaltable student_table'));
	$html .= html_writer::start_tag('tr');
	$html .= html_writer::start_tag('th', array('id' => 'td', 'class' => 'td'));
	$html .= html_writer::start_tag('b');
	$html .= "Student Name";
	$html .= html_writer::end_tag('b');
	$html .= html_writer::end_tag('th');
	$html .= html_writer::start_tag('th', array('id' => 'td2', 'class' => 'td2'));
	$html .= html_writer::start_tag('b');
	$html .= "Last Course Access";
	$html .= html_writer::end_tag('b');
	$html .= html_writer::end_tag('th');
	$html .= html_writer::start_tag('th', array('id' => 'td', 'class' => 'td'));
	$html .= html_writer::start_tag('b');
	$html .= "All logs";
	$html .= html_writer::end_tag('b');
	$html .= html_writer::end_tag('th');
	$html .= html_writer::start_tag('th', array('id' => 'td2', 'class' => 'td2'));
	$html .= html_writer::start_tag('b');
	$html .= "Today's log";
	$html .= html_writer::end_tag('b');
	$html .= html_writer::end_tag('th');
	$html .= html_writer::end_tag('tr');
	
	$i = 0;
	foreach ($result as $records) {
		$html .= html_writer::start_tag('tr');
		$html .= html_writer::start_tag('td', array('id' => 'td', 'class' => 'td'));
		$html .= $records->username;
		$html .= html_writer::end_tag('td');
		$html .= html_writer::start_tag('td', array('id' => 'td2', 'class' => 'td2'));
		$html .= $time[$i];
		$html .= html_writer::end_tag('td');
		$html .= html_writer::start_tag('td', array('id' => 'td', 'class' => 'td'));
		$html .= html_writer::start_tag('a', array('href'=> $link . "?id=". $records->id. "&course=" . $courseid . "&mode=today"));
		$html .= "View ". $records->username. "'s Today Logs";
		$html .= html_writer::end_tag('a');
		$html .= html_writer::end_tag('td');
		$html .= html_writer::start_tag('td', array('id' => 'td2', 'class' => 'td2'));
		$html .= html_writer::start_tag('a', array('href'=> $link . "?id=". $records->id. "&course=" . $courseid . "&mode=all"));
		$html .= "View ". $records->username. "'s All Logs";
		$html .= html_writer::end_tag('a');
		$html .= html_writer::end_tag('td');
		$html .= html_writer::end_tag('tr');
		++$i;
	}
	$html .= html_writer::end_tag('table');
	$html .= html_writer::end_tag('div');
	echo $html;
	echo $OUTPUT->footer();
?>
