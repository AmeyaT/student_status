<?php

defined('MOODLE_INTERNAL') || die();

$plugin->version   = 2015051100;
$plugin->requires  = 2015050500;
$plugin->component = 'block_student_status';
