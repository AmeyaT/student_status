<?php


defined('MOODLE_INTERNAL') || die();


class block_student_status extends block_base {
	public function init() {
		$this->title = get_string('blockname', 'block_student_status');
	}
	public function applicable_formats() {
		return array ('course-view' => true);
	}
	public function get_content_type() {
		return BLOCK_TYPE_TEXT;
	}
	public function get_content() {
		if ($this->content !== null) {
			return $this->content;
		}

		$this->content = new stdClass;
		$this->content->text = 'Click to view users';
		$this->content->footer = 'Footer here..';
		
		global $COURSE, $DB;
		$url = new moodle_url('/blocks/student_status/view.php', array('blockid' => $this->instance->id, 'courseid' => $COURSE->id));
		$this->content->footer = html_writer::link($url, get_string('addpage', 'block_student_status'));
		return $this->content;
	}
   	public function hide_header() {
		return false;
	}
	public function instance_delete() {
		global $DB;
		$DB->delete_records('block_student_status', array('blockid' => $this->instance->id));
	}
}
