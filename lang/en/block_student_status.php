<?php

$string['student_status:addinstance'] = 'Add a new student_status block';
$string['student_status:myaddinstance'] = 'Add a new student status block to Dashboard';
$string['blockname'] = 'Student Status';
$string['pluginname'] = 'Student Status block';
$string['student_status'] = 'Student Status';
$string['student_statussettings'] = 'Settings for student status block';
$string['addpage'] = 'View page';


